class Point {
    constructor(x,y){
        this.x = x;
        this.y = y;
    }
}

class Rectangle {
    constructor(x,y,w,h){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    // is point anywhere in rectangle
    contains(point) {
        return (point.x > this.x - this.w &&
                point.x < this.x + this.w &&
                point.y > this.y - this.h &&
                point.y < this.y + this.h
        )
    }
}


class QuadTree {
    constructor(boundary, n) {
        this.boundary = boundary;
        this.capacity = n;
        this.points = [];
        this.divided = false;
    }

    insert(point) {
        //console.log('insering point');

        if (!this.boundary.contains(point)) {
            return;
        }
        if (this.points.length < this.capacity) {
            this.points.push(point);
        }
        else {
            if(!this.divided) {
                this.subdivide();
            }
            this.northwest.insert(point);
            this.northeast.insert(point);
            this.southeast.insert(point);
            this.southwest.insert(point);
        }

    }

    subdivide() {

        let nw = new Rectangle(this.boundary.x + this.boundary.w / 2, this.boundary.y - this.boundary.y / 2, this.boundary.w / 2, this.boundary.h / 2);
        this.northwest = new QuadTree(nw);

        let ne = new Rectangle(this.boundary.x - this.boundary.w / 2, this.boundary.y - this.boundary.y / 2, this.boundary.w / 2, this.boundary.h / 2);
        this.northeast = new QuadTree(ne);

        let se = new Rectangle(this.boundary.x + this.boundary.w / 2, this.boundary.y + this.boundary.y / 2, this.boundary.w / 2, this.boundary.h / 2);
        this.southeast = new QuadTree(se);

        let sw = new Rectangle(this.boundary.x - this.boundary.w / 2, this.boundary.y + this.boundary.y / 2, this.boundary.w / 2, this.boundary.h / 2);
        this.southwest = new QuadTree(sw);

        this.divided = true;
    }
}

function setup() {
    createCanvas(400, 400);
    let boundary = new Rectangle(200,200,200,200);
    let qt = new QuadTree(boundary, 4);

    for  (let i=0; i < 500; i++) {
        let p = new Point(random(width), random(height));
        qt.insert(p);
    }
    console.log(qt);
}
